# Object Detection on Video using Faster R-CNN

This code performs object detection on a video using Faster R-CNN with a MobileNetV3 backbone. 
It detects objects in each frame of the video and draws bounding boxes around them.

Also it comes with different .py files that allow one to train their own detection model and a dataset of 900 pictures, 
600 for training and 300 for testing.

## Prerequisites

- Python 3.x
- OpenCV
- NumPy
- PyTorch
- Cuda (for better performance)


## Overview

This code utilizes the Faster R-CNN (Region-based Convolutional Neural Network) algorithm with a MobileNetV3 backbone for 
object detection. 
Faster R-CNN is a popular framework for object detection that consists of two main components: 
a region proposal network (RPN) and a detection network. 
The RPN generates candidate object regions, while the detection network classifies the objects and refines the bounding box predictions.

The MobileNetV3 backbone is a lightweight and efficient convolutional neural network architecture designed for mobile and embedded devices. 
It provides a good trade-off between accuracy and computational efficiency, making it suitable for real-time object detection tasks.

## Pros & Cons 

--  Faster R-CNN with MobileNetV3 Backbone:

Pros:

Faster R-CNN is a powerful object detection algorithm known for its accuracy and efficiency.
MobileNetV3 is a lightweight and efficient backbone architecture that strikes a good balance between accuracy and computational efficiency.
The combination of Faster R-CNN with MobileNetV3 allows for real-time object detection on resource-constrained devices.
The pretrained model used in the code eliminates the need for training from scratch, enabling quick and effective object detection.

Cons:

Although Faster R-CNN with MobileNetV3 provides accurate results, it may not achieve the same level of accuracy as more complex and resource-intensive architectures.
The performance of the model depends on the quality and diversity of the training dataset. Inadequate training data may result in suboptimal performance.

--  Video and Frame Manipulation using OpenCV:

Pros:

OpenCV provides a comprehensive set of tools and functions for video and frame manipulation.
The cv2.VideoCapture class allows easy access to video frames, frame properties, and playback control.
Frame skipping helps reduce the number of frames processed, leading to faster inference and smoother video playback.
Resizing frames allows for faster processing and facilitates compatibility with models that require specific input dimensions.
Drawing bounding boxes and class labels using OpenCV enables visual representation of detected objects.

Cons:

Frame skipping may result in missed detections if important objects appear in skipped frames.
Resizing frames can introduce minor distortions or loss of details, particularly when resizing to significantly smaller dimensions.

--  CUDA for GPU Acceleration:

Pros:

Utilizing CUDA and running the model on a GPU can significantly speed up the object detection process.
GPUs excel at parallel computations, allowing for faster inference compared to running on a CPU alone.
Faster inference enables real-time object detection in videos and improves overall performance.
Cons:

CUDA requires compatible NVIDIA GPUs and the installation of appropriate GPU drivers and CUDA toolkit.
Running the code on systems without a compatible GPU or CUDA support will result in falling back to CPU, which may be slower.

## Usage

1. Prepare the video file: Place your video file in the `data/inference_data/` directory.

2. Run the script with the following command:

python inference_video.py --input ../input/inference_data/video_1_trimmed_1.mp4

3. Adjust the optional arguments as needed:

- `-rs` or `--resize`: Provide an integer to resize the image (e.g., 300 will resize the image to 300x300).
- `-fs` or `--frame_skip`: Number of frames to skip between detections.

4. During video playback, press the "d" key to dynamically increase the number of frames skipped or "a" to decrease it. 
This can be useful for faster video playback.

5. The processed video with bounding boxes will be saved in the `inference_outputs/videos/` directory.

## Video and Frame Manipulation

This code utilizes OpenCV (cv2) for video and frame manipulation. Here are some key methods and techniques used:

-   `cv2.VideoCapture`: This class is used to capture video frames from a file. 
    It provides methods to read frames, retrieve frame properties, and check if the video capture is successful.

-   Frame Skipping: The code allows skipping frames to reduce the number of frames processed for object detection. 
    The `-fs` or `--frame_skip` argument determines the number of frames to skip between detections. 
    Additionally, during video playback, pressing the "d" key dynamically increases the number of frames skipped for faster video playback,
    "a" does the opposite.

-   Image Resizing: The `-rs` or `--resize` argument can be used to resize the input frames to a specific size. 
    Resizing frames can be useful for faster processing or when the model requires a specific input size.

-   Bounding Box Drawing: The detected objects are visualized by drawing bounding boxes around them using OpenCV. 
    The class labels and confidence scores are also displayed on top of the bounding boxes.

## Model

The object detection model used in this code is based on the Faster R-CNN architecture with a MobileNetV3 backbone. 
The model has been trained on a specific dataset to detect a set of predefined classes. 
The model weights are loaded from the `outputs/last_model.pth` file.

## Acknowledgements

This code is based on the Faster R-CNN implementation with MobileNetV3 backbone. 
The model and weights were trained on the dataset mentioned in the configuration file.

## Code and data source

https://debuggercafe.com/traffic-sign-detection-using-pytorch-and-pretrained-faster-rcnn-model/
by: Sovit Ranjan Rath

